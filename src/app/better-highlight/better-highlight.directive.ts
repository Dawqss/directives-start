import {
  Directive,
  Renderer2,
  OnInit,
  ElementRef,
  HostListener,
  HostBinding,
  Input
} from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit {

  constructor(private element: ElementRef, private render: Renderer2) { }

  ngOnInit() {
    this.backgroundColor = 'yellow';

  }
  @Input() defaultColor: string = 'transparent';
  @Input() highlighColor: string = 'blue';
  @HostBinding('style.backgroundColor') backgroundColor: string = this.defaultColor;

  @HostListener('o') mouseHover(eventData: Event) {
    this.backgroundColor = this.highlighColor;
  }
  @HostListener('mouseleave') mouseHover2(eventData: Event) {
    this.backgroundColor = this.defaultColor;
  }
}
