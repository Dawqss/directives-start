import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
    selector: '[appHighLight]'
})

export class BasicHighlightDirective implements OnInit {
    constructor(private ref: ElementRef) {}

    ngOnInit () {
        this.ref.nativeElement.className.backgroundColor = 'green';
    }
}